describe('SPA app', () => {
    beforeEach(() => {
        cy.visit('https://localhost:4200');
    });

    it('Should find announcement', () => {
        let code = '000003';
        let title = 'Ethereum za Shiba coin';
        let description = 'Ogłoszenie testowe';
        let fromAmount = '0.001';
        let toAmount = '1000000';

        cy.url().should('include', 'login');

        cy.get('button[id=announcement]').click();

        cy.url().should('include', 'main/announcements');

        cy.get('input').type(title);

        cy.get('a[id=code]').click();

        cy.url().should('include', 'main/announcements/000003');

        cy.get('#details').get('div').within(() => {
            cy.get('p').contains(code);
            cy.get('p').contains(title);
            cy.get('p').contains(description);
            cy.get('p').contains(fromAmount);
            cy.get('p').contains(toAmount);
        });
    });

    it('Should create announcement', () => {
        let title = 'Nowy tytuł';
        let description = 'Nowy opis';
        let fromCryptocurrency = 'Bitcoin/BTC';
        let fromAmount = '12.23';
        let toCryptocurrency = 'Ethereum/ERC20';
        let toAmount = '24.56';
        let location = 'Warszawa';

        cy.visit('https://localhost:4200');

        cy.get('input[name=Login]').type('username2');
        cy.get('input[name=Password]').type('password');
        cy.get('button[id=loginButton]').click();

        cy.url().should('include', 'main');

        cy.get('button[name=Announcements]').click();

        cy.url().should('include', 'main/announcements');

        cy.get('button[name=Add]').click();

        cy.url().should('include', 'main/announcements/add');

        cy.get('input[name=Title]').type(title);
        cy.get('input[name=Description]').type(description);
        cy.get('select[name=FromCryptocurrency]').select(fromCryptocurrency);
        cy.get('input[name=FromAmount]').type(fromAmount);
        cy.get('select[name=ToCryptocurrency]').select(toCryptocurrency);
        cy.get('input[name=ToAmount]').type(toAmount);
        cy.get('select[name=Location]').select(location);
        cy.get('button[name=Add]').click();

        cy.get('#details').get('div').within(() => {
            cy.get('p').contains(title);
            cy.get('p').contains(description);
            cy.get('p').contains('Bitcoin/BTC');
            cy.get('p').contains(fromAmount);
            cy.get('p').contains(toAmount);
            cy.get('p').contains('Ethereum/ERC20');
        });

        cy.get('button[name=Delete]').click();
        cy.get('button[name=Yes]').click();
    });
});
