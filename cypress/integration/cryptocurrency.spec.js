describe('SPA app', () => {
    beforeEach(() => {
        cy.visit('https://localhost:4200');

        cy.get('input[name=Login]').type('username1');
        cy.get('input[name=Password]').type('password');
        cy.get('button[id=loginButton]').click();

        cy.url().should('include', 'main');
    });

    it('Should create and delete new cryptocurrency', () => {
        const name = 'Test';
        const code = 'CRYP';
        const contract = '0x123456789';
        const decimals = '13';
        const network = 'BTC';

        cy.get('button[name=Cryptocurrencies]').click();

        cy.url().should('include', 'main/cryptocurrencies');

        cy.get('button[name=Add]').click();

        cy.get('input[name=Name]').type(name);
        cy.get('input[name=Code]').type(code);
        cy.get('input[name=Contract]').type(contract);
        cy.get('input[name=Decimals]').type(decimals);
        cy.get('select').select(network);
        cy.get('button[name=Add]').click();

        cy.url().should('include', 'main/cryptocurrencies/CRYP');

        cy.get('#details').get('div').within(() => {
            cy.get('p').contains(name);
            cy.get('p').contains(code);
            cy.get('p').contains(contract);
            cy.get('p').contains(decimals);
        });

        cy.get('button[name=Delete]').click();
        cy.get('button[name=Yes]').click();
    });
});
