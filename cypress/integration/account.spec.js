describe('SPA app', () => {
    beforeEach(() => {
        cy.visit('https://localhost:4200');

        cy.get('input[name=Login]').type('username1');
        cy.get('input[name=Password]').type('password');
        cy.get('button[id=loginButton]').click();

        cy.url().should('include', 'main');
    });

    it('Should show my account', () => {
        cy.get('button[name=Account]').click();

        cy.url().should('include', 'main/personaldata/own');

        cy.get('#details').get('div').within(() => {
            cy.get('p').contains('Jan');
            cy.get('p').contains('Kowalski');
            cy.get('p').contains('123456789');
            cy.get('p').contains('jan.kowalski@web.com');
        });
    });
});
