describe('SPA app', () => {
    beforeEach(() => {
        cy.visit('https://localhost:4200');
    });

    it('Should log in', () => {
        cy.url().should('include', 'login');

        cy.get('input[name=Login]').type('username1');
        cy.get('input[name=Password]').type('password');
        cy.get('button[id=loginButton]').click();

        cy.url().should('include', 'main');
    });
});
