describe('SPA app', () => {
    beforeEach(() => {
        cy.visit('https://localhost:4200');

        cy.get('input[name=Login]').type('username1');
        cy.get('input[name=Password]').type('password');
        cy.get('button[id=loginButton]').click();

        cy.url().should('include', 'main');
    });

    it('Should create and delete new network', () => {
        cy.get('button[name=Networks]').click();

        cy.url().should('include', 'main/networks');

        cy.get('button[name=Add]').click();

        cy.get('input[name=Name]').type('Test');
        cy.get('input[name=Code]').type('NET');
        cy.get('button[name=Add]').click();

        cy.url().should('include', 'main/networks/NET');

        cy.get('#details').get('div').within(() => {
            cy.get('p').contains('NET');
            cy.get('p').contains('Test');
        });

        cy.get('button[name=Delete]').click();
        cy.get('button[name=Yes]').click();
    });
});
