describe('SPA app', () => {
    beforeEach(() => {
        cy.visit('https://localhost:4200');

        cy.get('input[name=Login]').type('username1');
        cy.get('input[name=Password]').type('password');
        cy.get('button[id=loginButton]').click();

        cy.url().should('include', 'main');
    });

    it('Should create and delete new location', () => {
        cy.get('button[name=Locations]').click();

        cy.url().should('include', 'main/locations');

        cy.get('button[name=Add]').click();

        cy.get('input[name=Name]').type('Test');
        cy.get('input[name=Code]').type('LOC');
        cy.get('button[name=Add]').click();

        cy.url().should('include', 'main/locations/LOC');

        cy.get('#details').get('div').within(() => {
            cy.get('p').contains('LOC');
            cy.get('p').contains('Test');
        });

        cy.get('button[name=Delete]').click();
        cy.get('button[name=Yes]').click();
    });
});
