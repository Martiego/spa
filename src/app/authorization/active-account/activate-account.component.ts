import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../core/service/auth.service';

@Component({
    selector: 'app-active-account',
    templateUrl: './activate-account.component.html',
    styleUrls: ['./activate-account.component.less']
})
export class ActivateAccountComponent implements OnInit {

    oneTimeCode: string;

    constructor(private route: ActivatedRoute,
                private authService: AuthService) {
        this.oneTimeCode = this.route.snapshot.paramMap.get('oneTimeCode') as string;

        this.authService.activateAccount(this.oneTimeCode);
    }

    ngOnInit(): void {
    }

}
