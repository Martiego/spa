import { Component, OnInit } from '@angular/core';
import { PersonalData } from '../../core/model/PersonalData';
import { defaultPersonalData } from '../../core/model/default-model/default-personal-data';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../core/service/auth.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.less']
})
export class RegisterComponent implements OnInit {

    personalData: PersonalData = defaultPersonalData();

    form = new FormGroup({
        firstName: new FormControl('', [Validators.required]),
        lastName: new FormControl('', [Validators.required]),
        email: new FormControl('', [Validators.required]),
        phoneNumber: new FormControl('', [Validators.required]),
        login: new FormControl('', [Validators.required]),
        password: new FormControl('', [Validators.required, Validators.minLength(8)]),
    });

    constructor(public authService: AuthService) {
    }

    ngOnInit(): void {
    }

    register(): void {
        if (this.authService.isAdmin()) {
            this.authService.registerAdmin(this.personalData);
        } else {
            this.authService.registerClient(this.personalData);
        }
    }

}
