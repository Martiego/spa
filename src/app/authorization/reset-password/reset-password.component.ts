import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../core/service/auth.service';

@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.less']
})
export class ResetPasswordComponent implements OnInit {

    login = '';

    form = new FormGroup({
        login: new FormControl('', [Validators.required]),
    });

    constructor(private authService: AuthService) {
    }

    ngOnInit(): void {
    }

    resetPassword(): void {
        this.authService.resetPassword(this.login);
    }
}
