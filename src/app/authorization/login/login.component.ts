import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/service/auth.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

    login = '';

    password = '';

    form = new FormGroup({
        login: new FormControl('', [Validators.required]),
        password: new FormControl('', [Validators.required]),
    });

    constructor(private authService: AuthService,
                private router: Router) {
    }

    ngOnInit() {
        if (this.authService.isLoggedIn()) {
            this.router.navigateByUrl('/main');
        }
    }

    logIn(): void {
        this.authService.logIn(this.login, this.password);
    }
}
