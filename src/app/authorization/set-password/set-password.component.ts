import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../core/service/auth.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { defaultAccount } from '../../core/model/default-model/default-account';

@Component({
    selector: 'app-set-password',
    templateUrl: './set-password.component.html',
    styleUrls: ['./set-password.component.less']
})
export class SetPasswordComponent implements OnInit {

    oneTimeCode: string;

    account = defaultAccount();

    form = new FormGroup({
        login: new FormControl('', [Validators.required]),
        password: new FormControl('', [Validators.required, Validators.minLength(8)]),
    });

    constructor(private route: ActivatedRoute,
                private authService: AuthService) {
        this.oneTimeCode = this.route.snapshot.paramMap.get('oneTimeCode') as string;
    }

    ngOnInit(): void {
    }

    setNewPassword(): void {
        this.authService.setNewPassword(this.account, this.oneTimeCode);
    }
}
