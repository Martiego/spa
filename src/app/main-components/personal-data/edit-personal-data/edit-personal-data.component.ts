import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AbstractEditEntityComponent } from '../../abstract.edit-entity.component';
import { PersonalData } from '../../../core/model/PersonalData';
import { PersonalDataService } from '../../../core/service/personal-data.service';
import { defaultPersonalData } from '../../../core/model/default-model/default-personal-data';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-edit-personal-data',
    templateUrl: './edit-personal-data.component.html',
    styleUrls: ['./edit-personal-data.component.less']
})
export class EditPersonalDataComponent extends AbstractEditEntityComponent<PersonalData> implements OnInit {

    personalData: PersonalData = defaultPersonalData();

    form = new FormGroup({
        firstName: new FormControl('', [Validators.required]),
        lastName: new FormControl('', [Validators.required]),
        phoneNumber: new FormControl('', [Validators.required])
    });

    constructor(private activatedRoute: ActivatedRoute,
                personalDataService: PersonalDataService,
                router: Router) {
        super(personalDataService, router);
    }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe(data => {
            this.personalData = data.personalData;
        });

        this.personalData.code = this.activatedRoute.snapshot.params['code'];
    }

}
