import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PersonalData } from '../../../core/model/PersonalData';
import { AuthService } from '../../../core/service/auth.service';

@Component({
    selector: 'app-personal-data-table',
    templateUrl: './personal-data-table.component.html',
    styleUrls: ['./personal-data-table.component.less']
})
export class PersonalDataTableComponent implements OnInit {

    personalData: PersonalData[] = [];

    filteredPersonalData: PersonalData[] = [];

    constructor(private activatedRoute: ActivatedRoute,
                public authService: AuthService) {
    }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe(data => {
            this.personalData = data.personalData;
        });

        this.filteredPersonalData = this.personalData;
    }

    setFilteredValues($event: any) {
        this.filteredPersonalData = $event as PersonalData[];
    }
}
