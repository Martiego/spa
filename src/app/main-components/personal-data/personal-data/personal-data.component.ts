import { Component, OnInit } from '@angular/core';
import { AbstractDetailsComponent } from '../../abstract.details.component';
import { PersonalData } from '../../../core/model/PersonalData';
import { PersonalDataService } from '../../../core/service/personal-data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { defaultPersonalData } from '../../../core/model/default-model/default-personal-data';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PersonalDataMapper } from '../../../core/model/mapper/PersonalDataMapper';
import { AuthService } from '../../../core/service/auth.service';

@Component({
    selector: 'app-personal-data',
    templateUrl: './personal-data.component.html',
    styleUrls: ['./personal-data.component.less']
})
export class PersonalDataComponent extends AbstractDetailsComponent<PersonalData> implements OnInit {

    personalData: PersonalData = defaultPersonalData();

    constructor(private activatedRoute: ActivatedRoute,
                public authService: AuthService,
                service: PersonalDataService,
                router: Router,
                modalService: NgbModal,
                mapper: PersonalDataMapper) {
        super(service, router, modalService, mapper);
    }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe(data => {
            this.personalData = data.personalData;
        });
    }

    canDeactivateAccount(): boolean {
        return !!(this.personalData.account?.active && this.activatedRoute.snapshot.params['code'] !== 'own');
    }

    activateAccount(): void {
        this.authService.activateAccountByAdmin(this.personalData.account.username);
    }

}
