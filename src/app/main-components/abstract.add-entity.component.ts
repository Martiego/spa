import { AbstractService } from '../core/service/abstract.service';
import { Router } from '@angular/router';

export abstract class AbstractAddEntityComponent<D> {

    protected constructor(private service: AbstractService<D>,
                          private router: Router) {
    }

    add(entity: D): void {
        this.service.add(entity).subscribe(response => {
            this.router.navigateByUrl('main/' + response.headers.get('Location') || 'main');
        });
    }
}
