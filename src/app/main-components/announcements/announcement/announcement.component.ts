import { Component, OnInit } from '@angular/core';
import { AbstractDetailsComponent } from '../../abstract.details.component';
import { Announcement } from '../../../core/model/Announcement';
import { ActivatedRoute, Router } from '@angular/router';
import { AnnouncementService } from '../../../core/service/announcement.service';
import { defaultAnnouncement } from '../../../core/model/default-model/default-announcement';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AnnouncementMapper } from '../../../core/model/mapper/AnnouncementMapper';
import { AuthService } from '../../../core/service/auth.service';
import { ContactComponent } from '../contact/contact.component';

@Component({
    selector: 'app-announcement',
    templateUrl: './announcement.component.html',
    styleUrls: ['./announcement.component.less']
})
export class AnnouncementComponent extends AbstractDetailsComponent<Announcement> implements OnInit {

    announcement: Announcement = defaultAnnouncement();

    constructor(private activatedRoute: ActivatedRoute,
                public authService: AuthService,
                service: AnnouncementService,
                router: Router,
                modalService: NgbModal,
                mapper: AnnouncementMapper) {
        super(service, router, modalService, mapper);
    }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe(data => {
            this.announcement = data.announcement;
        });
    }

    canDeleteAnnouncement(): boolean {
        return this.authService.isAdmin() ||
            this.authService.isClient() && this.announcement.personalData?.account?.username === this.authService.getUsername();
    }

    contact(): void {
        const modalRef = this.modalService.open(ContactComponent);
        modalRef.componentInstance.personalData = this.announcement.personalData;
    }
}
