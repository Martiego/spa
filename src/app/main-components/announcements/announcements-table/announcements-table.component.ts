import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Announcement } from '../../../core/model/Announcement';
import { AuthService } from '../../../core/service/auth.service';
import { AnnouncementMapper } from '../../../core/model/mapper/AnnouncementMapper';

@Component({
    selector: 'app-announcements-table',
    templateUrl: './announcements-table.component.html',
    styleUrls: ['./announcements-table.component.less']
})
export class AnnouncementsTableComponent implements OnInit {

    announcements: Announcement[] = [];

    filteredAnnouncements: Announcement[] = [];

    constructor(private activatedRoute: ActivatedRoute,
                public authService: AuthService,
                public announcementMapper: AnnouncementMapper) {
    }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe(data => {
            this.announcements = data.announcements;
        });

        this.filteredAnnouncements = this.announcements;
    }

    setFilteredValues($event: any) {
        this.filteredAnnouncements = $event as Announcement[];
    }
}
