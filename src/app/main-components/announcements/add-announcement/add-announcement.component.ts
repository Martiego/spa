import { Component, OnInit } from '@angular/core';
import { AbstractAddEntityComponent } from '../../abstract.add-entity.component';
import { Announcement } from '../../../core/model/Announcement';
import { AnnouncementService } from '../../../core/service/announcement.service';
import { ActivatedRoute, Router } from '@angular/router';
import { defaultAnnouncement } from '../../../core/model/default-model/default-announcement';
import { Location } from '../../../core/model/Location';
import { Cryptocurrency } from '../../../core/model/Cryptocurrency';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-add-announcement',
    templateUrl: './add-announcement.component.html',
    styleUrls: ['./add-announcement.component.less']
})
export class AddAnnouncementComponent extends AbstractAddEntityComponent<Announcement> implements OnInit {

    announcement: Announcement = defaultAnnouncement();

    form = new FormGroup({
        title: new FormControl('', [Validators.required]),
        description: new FormControl('', [Validators.required]),
        fromCryptocurrency: new FormControl(''),
        fromAmount: new FormControl('', [Validators.required]),
        toCryptocurrency: new FormControl(''),
        toAmount: new FormControl('', [Validators.required]),
        location: new FormControl('', [Validators.required])
    });

    locations: Location[] = [];

    cryptocurrencies: Cryptocurrency[] = [];

    constructor(private activatedRoute: ActivatedRoute,
                service: AnnouncementService,
                router: Router) {
        super(service, router);
    }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe(data => {
            this.locations = data.locations;
            this.cryptocurrencies = data.cryptocurrencies;
        })
    }

    prepareCryptocurrency(cryptocurrency: Cryptocurrency): string {
        return cryptocurrency.name + '/' + cryptocurrency.network.code;
    }

}
