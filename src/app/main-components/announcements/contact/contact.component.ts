import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PersonalData } from '../../../core/model/PersonalData';
import { defaultPersonalData } from '../../../core/model/default-model/default-personal-data';
import { AbstractDetailsComponent } from '../../abstract.details.component';
import { PersonalDataService } from '../../../core/service/personal-data.service';
import { Router } from '@angular/router';
import { PersonalDataMapper } from '../../../core/model/mapper/PersonalDataMapper';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.less']
})
export class ContactComponent extends AbstractDetailsComponent<PersonalData> implements OnInit {

    @Input()
    personalData: PersonalData = defaultPersonalData();

    constructor(public activeModal: NgbActiveModal,
                service: PersonalDataService,
                router: Router,
                modalService: NgbModal,
                mapper: PersonalDataMapper) {
        super(service, router, modalService, mapper);
    }

    ngOnInit(): void {
    }

}
