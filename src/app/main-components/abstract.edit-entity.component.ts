import { AbstractService } from '../core/service/abstract.service';
import { Router } from '@angular/router';

export abstract class AbstractEditEntityComponent<D> {

    protected constructor(private service: AbstractService<D>,
                          private router: Router) {
    }

    edit(entity: D): void {
        this.service.edit(entity).subscribe();

        this.router.navigate(['main']);
    }
}
