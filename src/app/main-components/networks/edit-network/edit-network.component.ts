import { Component, OnInit } from '@angular/core';
import { Network } from '../../../core/model/Network';
import { AbstractEditEntityComponent } from '../../abstract.edit-entity.component';
import { NetworkService } from '../../../core/service/network.service';
import { ActivatedRoute, Router } from '@angular/router';
import { defaultNetwork } from '../../../core/model/default-model/default-network';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-edit-network',
    templateUrl: './edit-network.component.html',
    styleUrls: ['./edit-network.component.less']
})
export class EditNetworkComponent extends AbstractEditEntityComponent<Network> implements OnInit {

    network: Network = defaultNetwork();

    form = new FormGroup({
        name: new FormControl('', [Validators.required])
    });

    constructor(private activatedRoute: ActivatedRoute,
                networkService: NetworkService,
                router: Router) {
        super(networkService, router);
    }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe(data => {
           this.network = data.network;
        });
    }

}
