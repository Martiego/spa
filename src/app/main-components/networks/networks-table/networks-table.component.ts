import { Component, OnInit } from '@angular/core';
import { Network } from '../../../core/model/Network';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../core/service/auth.service';

@Component({
    selector: 'app-networks-table',
    templateUrl: './networks-table.component.html',
    styleUrls: ['./networks-table.component.less']
})
export class NetworksTableComponent implements OnInit {

    networks: Network[] = [];

    filteredNetworks: Network[] = [];

    constructor(private activatedRoute: ActivatedRoute,
                public authService: AuthService) {
    }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe(data => {
            this.networks = data.networks;
        })

        this.filteredNetworks = this.networks;
    }

    setFilteredValues($event: any) {
        this.filteredNetworks = $event as Network[];
    }
}
