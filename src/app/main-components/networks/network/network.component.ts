import { Component, OnInit } from '@angular/core';
import { Network } from '../../../core/model/Network';
import { defaultNetwork } from '../../../core/model/default-model/default-network';
import { ActivatedRoute, Router } from '@angular/router';
import { AbstractDetailsComponent } from '../../abstract.details.component';
import { NetworkService } from '../../../core/service/network.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NetworkMapper } from '../../../core/model/mapper/NetworkMapper';
import { AuthService } from '../../../core/service/auth.service';

@Component({
    selector: 'app-network',
    templateUrl: './network.component.html',
    styleUrls: ['./network.component.less']
})
export class NetworkComponent extends AbstractDetailsComponent<Network> implements OnInit {

    network: Network = defaultNetwork();

    constructor(private activatedRoute: ActivatedRoute,
                public authService: AuthService,
                service: NetworkService,
                router: Router,
                modalService: NgbModal,
                mapper: NetworkMapper) {
        super(service, router, modalService, mapper);
    }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe(data => {
            this.network = data.network;
        });
    }
}
