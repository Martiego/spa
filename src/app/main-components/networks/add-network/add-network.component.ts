import { Component, OnInit } from '@angular/core';
import { AbstractAddEntityComponent } from '../../abstract.add-entity.component';
import { Network } from '../../../core/model/Network';
import { NetworkService } from '../../../core/service/network.service';
import { Router } from '@angular/router';
import { defaultNetwork } from '../../../core/model/default-model/default-network';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-add-network',
    templateUrl: './add-network.component.html',
    styleUrls: ['./add-network.component.less']
})
export class AddNetworkComponent extends AbstractAddEntityComponent<Network> implements OnInit {

    network: Network = defaultNetwork();

    form = new FormGroup({
        name: new FormControl('', [Validators.required]),
        code: new FormControl('', [Validators.required, Validators.maxLength(6)])
    });

    constructor(service: NetworkService,
                router: Router) {
        super(service, router);
    }

    ngOnInit(): void {
    }

}
