import { AbstractService } from '../core/service/abstract.service';
import { Router } from '@angular/router';
import { ConfirmComponent } from '../utils/confirm/confirm.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractMapper } from '../core/model/mapper/AbstractMapper';

export abstract class AbstractDetailsComponent<D> {

    protected constructor(private service: AbstractService<D>,
                          private router: Router,
                          protected modalService: NgbModal,
                          private mapper: AbstractMapper<D>) {
    }

    delete(code: string): void {
        const modalRef = this.modalService.open(ConfirmComponent);

        modalRef.result.then(result => {
            if (result === 'YES') {
                this.service.delete(code).subscribe(_ => {
                    this.router.navigate(['main']);
                });
            }
        }).catch(_ => _);
    }

    values(model: D): string[] {
        return this.mapper.values(model);
    }

    labels(): string[] {
        return this.mapper.labels();
    }
}
