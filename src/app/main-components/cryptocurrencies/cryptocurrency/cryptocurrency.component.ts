import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Cryptocurrency } from '../../../core/model/Cryptocurrency';
import { defaultCryptocurrency } from '../../../core/model/default-model/default-cryptocurrency';
import { AbstractDetailsComponent } from '../../abstract.details.component';
import { CryptocurrencyService } from '../../../core/service/cryptocurrency.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CryptocurrencyMapper } from '../../../core/model/mapper/CryptocurrencyMapper';
import { AuthService } from '../../../core/service/auth.service';

@Component({
    selector: 'app-cryptocurrency',
    templateUrl: './cryptocurrency.component.html',
    styleUrls: ['./cryptocurrency.component.less']
})
export class CryptocurrencyComponent extends AbstractDetailsComponent<Cryptocurrency> implements OnInit {

    cryptocurrency: Cryptocurrency = defaultCryptocurrency();

    constructor(private activatedRoute: ActivatedRoute,
                public authService: AuthService,
                service: CryptocurrencyService,
                router: Router,
                modalService: NgbModal,
                mapper: CryptocurrencyMapper) {
        super(service, router, modalService, mapper);
    }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe(data => {
            this.cryptocurrency = data.cryptocurrency;
        });
    }

}
