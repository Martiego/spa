import { Component, OnInit } from '@angular/core';
import { AbstractAddEntityComponent } from '../../abstract.add-entity.component';
import { Cryptocurrency } from '../../../core/model/Cryptocurrency';
import { Network } from '../../../core/model/Network';
import { defaultCryptocurrency } from '../../../core/model/default-model/default-cryptocurrency';
import { CryptocurrencyService } from '../../../core/service/cryptocurrency.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-add-cryptocurrency',
    templateUrl: './add-cryptocurrency.component.html',
    styleUrls: ['./add-cryptocurrency.component.less']
})
export class AddCryptocurrencyComponent extends AbstractAddEntityComponent<Cryptocurrency> implements OnInit {

    networks: Network[] = [];

    cryptocurrency: Cryptocurrency = defaultCryptocurrency();

    form = new FormGroup({
        name: new FormControl('', [Validators.required]),
        code: new FormControl('', [Validators.required, Validators.maxLength(4)]),
        network: new FormControl('', [Validators.required]),
        contract: new FormControl('', [Validators.required]),
        decimals: new FormControl('', [Validators.required])
    });

    constructor(private activatedRoute: ActivatedRoute,
                service: CryptocurrencyService,
                router: Router) {
        super(service, router);
    }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe(data => {
            this.networks = data.networks;
        })
    }

}
