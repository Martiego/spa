import { Component, OnInit } from '@angular/core';
import { Cryptocurrency } from '../../../core/model/Cryptocurrency';
import { defaultCryptocurrency } from '../../../core/model/default-model/default-cryptocurrency';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AbstractEditEntityComponent } from '../../abstract.edit-entity.component';
import { ActivatedRoute, Router } from '@angular/router';
import { CryptocurrencyService } from '../../../core/service/cryptocurrency.service';

@Component({
    selector: 'app-edit-cryptocurrency',
    templateUrl: './edit-cryptocurrency.component.html',
    styleUrls: ['./edit-cryptocurrency.component.less']
})
export class EditCryptocurrencyComponent extends AbstractEditEntityComponent<Cryptocurrency> implements OnInit {

    cryptocurrency: Cryptocurrency = defaultCryptocurrency();

    form = new FormGroup({
        name: new FormControl('', [Validators.required]),
        contract: new FormControl('', [Validators.maxLength(64)]),
        decimals: new FormControl('', [Validators.required])
    });

    constructor(private activatedRoute: ActivatedRoute,
                cryptocurrencyService: CryptocurrencyService,
                router: Router) {
        super(cryptocurrencyService, router);
    }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe(data => {
            this.cryptocurrency = data.cryptocurrency;
        });
    }

}
