import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Cryptocurrency } from '../../../core/model/Cryptocurrency';
import { AuthService } from '../../../core/service/auth.service';

@Component({
    selector: 'app-cryptocurrencies-table',
    templateUrl: './cryptocurrencies-table.component.html',
    styleUrls: ['./cryptocurrencies-table.component.less']
})
export class CryptocurrenciesTableComponent implements OnInit {

    cryptocurrencies: Cryptocurrency[] = [];

    filteredCryptocurrencies: Cryptocurrency[] = [];

    constructor(private activatedRoute: ActivatedRoute,
                public authService: AuthService) {
    }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe(data => {
            this.cryptocurrencies = data.cryptocurrencies;
        });

        this.filteredCryptocurrencies = this.cryptocurrencies;
    }

    setFilteredValues($event: any) {
        this.filteredCryptocurrencies = $event as Cryptocurrency[];
    }
}
