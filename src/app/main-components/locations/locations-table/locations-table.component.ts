import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '../../../core/model/Location';
import { AuthService } from '../../../core/service/auth.service';
import { Network } from '../../../core/model/Network';

@Component({
    selector: 'app-locations-table',
    templateUrl: './locations-table.component.html',
    styleUrls: ['./locations-table.component.less']
})
export class LocationsTableComponent implements OnInit {

    locations: Location[] = [];

    filteredLocations: Location[] = [];

    constructor(private activatedRoute: ActivatedRoute,
                public authService: AuthService) {
    }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe(data => {
            this.locations = data.locations;
        });

        this.filteredLocations = this.locations;
    }

    setFilteredValues($event: any) {
        this.filteredLocations = $event as Network[];
    }
}
