import { Component, OnInit } from '@angular/core';
import { defaultLocation } from '../../../core/model/default-model/default-location';
import { Location } from '../../../core/model/Location';
import { AbstractAddEntityComponent } from '../../abstract.add-entity.component';
import { LocationService } from '../../../core/service/location.service';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-add-location',
    templateUrl: './add-location.component.html',
    styleUrls: ['./add-location.component.less']
})
export class AddLocationComponent extends AbstractAddEntityComponent<Location> implements OnInit {

    location: Location = defaultLocation();

    form = new FormGroup({
        name: new FormControl('', [Validators.required]),
        code: new FormControl('', [Validators.required, Validators.maxLength(3)])
    })

    constructor(service: LocationService,
                router: Router) {
        super(service, router);
    }

    ngOnInit(): void {
    }

}
