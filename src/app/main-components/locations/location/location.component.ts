import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '../../../core/model/Location';
import { defaultLocation } from '../../../core/model/default-model/default-location';
import { LocationMapper } from '../../../core/model/mapper/LocationMapper';
import { LocationService } from '../../../core/service/location.service';
import { AbstractDetailsComponent } from '../../abstract.details.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../../core/service/auth.service';

@Component({
    selector: 'app-location',
    templateUrl: './location.component.html',
    styleUrls: ['./location.component.less']
})
export class LocationComponent extends AbstractDetailsComponent<Location> implements OnInit {

    location: Location = defaultLocation();

    constructor(private activatedRoute: ActivatedRoute,
                public authService: AuthService,
                service: LocationService,
                router: Router,
                modalService: NgbModal,
                mapper: LocationMapper) {
        super(service, router, modalService, mapper);
        Object.keys(this.location);
    }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe(data => {
            this.location = data.location;
        })
    }
}
