import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '../../../core/model/Location';
import { defaultLocation } from '../../../core/model/default-model/default-location';
import { AbstractEditEntityComponent } from '../../abstract.edit-entity.component';
import { LocationService } from '../../../core/service/location.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-edit-location',
    templateUrl: './edit-location.component.html',
    styleUrls: ['./edit-location.component.less']
})
export class EditLocationComponent extends AbstractEditEntityComponent<Location> implements OnInit {

    location: Location = defaultLocation();

    form = new FormGroup({
        name: new FormControl('', [Validators.required])
    });

    constructor(private activatedRoute: ActivatedRoute,
                locationService: LocationService,
                router: Router) {
        super(locationService, router);
    }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe(data => {
            this.location = data.location;
        });
    }

}
