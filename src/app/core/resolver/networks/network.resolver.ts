import { Network } from '../../model/Network';
import { Injectable } from '@angular/core';
import { NetworkService } from '../../service/network.service';
import { AbstractGetResolver } from '../abstract.get.resolver';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class NetworkResolver extends AbstractGetResolver<Network> {

    constructor(service: NetworkService,
                cookieService: CookieService) {
        super(service, cookieService);
    }
}
