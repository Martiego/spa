import { Injectable } from '@angular/core';
import { Network } from '../../model/Network';
import { NetworkService } from '../../service/network.service';
import { AbstractGetAllResolver } from '../abstract.get-all.resolver';

@Injectable()
export class NetworksResolver extends AbstractGetAllResolver<Network> {

    constructor(service: NetworkService) {
        super(service);
    }
}
