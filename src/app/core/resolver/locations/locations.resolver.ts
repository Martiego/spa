import { Injectable } from '@angular/core';
import { Location } from '../../model/Location';
import { LocationService } from '../../service/location.service';
import { AbstractGetAllResolver } from '../abstract.get-all.resolver';

@Injectable()
export class LocationsResolver extends AbstractGetAllResolver<Location> {

    constructor(service: LocationService) {
        super(service);
    }
}
