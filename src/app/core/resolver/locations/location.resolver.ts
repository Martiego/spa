import { Injectable } from '@angular/core';
import { Location } from '../../model/Location';
import { LocationService } from '../../service/location.service';
import { AbstractGetResolver } from '../abstract.get.resolver';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class LocationResolver extends AbstractGetResolver<Location> {

    constructor(service: LocationService,
                cookieService: CookieService) {
        super(service, cookieService);
    }
}
