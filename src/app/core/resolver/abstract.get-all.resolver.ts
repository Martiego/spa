import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AbstractService } from '../service/abstract.service';

export abstract class AbstractGetAllResolver<D> implements Resolve<D[]> {

    protected constructor(protected service: AbstractService<D>) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<D[]> {
        return this.service.getAll();
    }
}
