import { AbstractGetAllResolver } from '../abstract.get-all.resolver';
import { PersonalDataService } from '../../service/personal-data.service';
import { Injectable } from '@angular/core';
import { PersonalData } from '../../model/PersonalData';

@Injectable()
export class AllPersonalDataResolver extends AbstractGetAllResolver<PersonalData> {

    constructor(service: PersonalDataService) {
        super(service);
    }
}
