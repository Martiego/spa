import { AbstractGetResolver } from '../abstract.get.resolver';
import { PersonalData } from '../../model/PersonalData';
import { Injectable } from '@angular/core';
import { PersonalDataService } from '../../service/personal-data.service';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class PersonalDataResolver extends AbstractGetResolver<PersonalData> {

    constructor(service: PersonalDataService,
                cookieService: CookieService) {
        super(service, cookieService);
    }

}
