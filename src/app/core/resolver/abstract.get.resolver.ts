import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { AbstractService } from '../service/abstract.service';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

export abstract class AbstractGetResolver<D> implements Resolve<D> {

    protected constructor(protected service: AbstractService<D>,
                          private cookieService: CookieService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<D> {
        const code = route.params['code'];
        this.clearEtag();

        return this.service.get(code).pipe(
            tap(response => this.cookieService.set('eTag', response.headers.get('eTag') as string)),
            map(response => response.body as D)
        );
    }

    private clearEtag(): void {
        this.cookieService.set('eTag', '');
    }

}
