import { Injectable } from '@angular/core';
import { AbstractGetAllResolver } from '../abstract.get-all.resolver';
import { Announcement } from '../../model/Announcement';
import { AnnouncementService } from '../../service/announcement.service';

@Injectable()
export class AnnouncementsResolver extends AbstractGetAllResolver<Announcement> {

    constructor(service: AnnouncementService) {
        super(service);
    }
}
