import { Injectable } from '@angular/core';
import { Announcement } from '../../model/Announcement';
import { AbstractGetResolver } from '../abstract.get.resolver';
import { AnnouncementService } from '../../service/announcement.service';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class AnnouncementResolver extends AbstractGetResolver<Announcement> {

    constructor(service: AnnouncementService,
                cookieService: CookieService) {
        super(service, cookieService);
    }
}
