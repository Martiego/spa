import { CryptocurrencyService } from '../../service/cryptocurrency.service';
import { Injectable } from '@angular/core';
import { Cryptocurrency } from '../../model/Cryptocurrency';
import { AbstractGetAllResolver } from '../abstract.get-all.resolver';

@Injectable()
export class CryptocurrenciesResolver extends AbstractGetAllResolver<Cryptocurrency> {

    constructor(service: CryptocurrencyService) {
        super(service);
    }
}
