import { Cryptocurrency } from '../../model/Cryptocurrency';
import { CryptocurrencyService } from '../../service/cryptocurrency.service';
import { Injectable } from '@angular/core';
import { AbstractGetResolver } from '../abstract.get.resolver';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class CryptocurrencyResolver extends AbstractGetResolver<Cryptocurrency> {

    constructor(service: CryptocurrencyService,
                cookieService: CookieService) {
        super(service, cookieService);
    }
}
