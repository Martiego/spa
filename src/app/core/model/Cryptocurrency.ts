import { Network } from './Network';

export interface Cryptocurrency {
    name: string;
    code: string;
    network: Network;
    contract?: string;
    decimals: number;
}
