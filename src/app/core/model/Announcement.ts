import { Cryptocurrency } from './Cryptocurrency';
import { Location } from './Location';
import { PersonalData } from './PersonalData';

export interface Announcement {
    code: string;
    title: string;
    description: string;
    fromCryptocurrency: Cryptocurrency;
    fromAmount: number;
    toCryptocurrency: Cryptocurrency;
    toAmount: number;
    location: Location;
    available: boolean;
    personalData?: PersonalData;
}
