import { Announcement } from '../Announcement';
import { defaultCryptocurrency } from './default-cryptocurrency';
import { defaultLocation } from './default-location';
import { defaultPersonalData } from './default-personal-data';

export function defaultAnnouncement(): Announcement {
    return {
        code: '',
        title: '',
        description: '',
        fromCryptocurrency: defaultCryptocurrency(),
        fromAmount: 0,
        toCryptocurrency: defaultCryptocurrency(),
        toAmount: 0,
        location: defaultLocation(),
        available: true,
        personalData: defaultPersonalData()
    }
}
