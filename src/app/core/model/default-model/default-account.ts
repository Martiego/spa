import { Account } from '../Account';

export function defaultAccount(): Account {
    return {
        username: '',
        password: '',
        active: false
    }
}
