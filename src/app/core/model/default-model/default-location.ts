import { Location } from '../Location';

export function defaultLocation(): Location {
    return {
        name: '',
        code: ''
    }
}
