import { Cryptocurrency } from '../Cryptocurrency';
import { defaultNetwork } from './default-network';

export function defaultCryptocurrency(): Cryptocurrency {
    return {
        name: '',
        code: '',
        network: defaultNetwork(),
        contract: '',
        decimals: 0
    }
}
