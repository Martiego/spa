import { Network } from '../Network';

export function defaultNetwork(): Network {
    return {
        name: '',
        code: ''
    }
}
