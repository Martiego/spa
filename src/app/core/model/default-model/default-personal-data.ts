import { PersonalData } from '../PersonalData';
import { defaultAccount } from './default-account';
import { defaultLocation } from './default-location';

export function defaultPersonalData(): PersonalData {
    return {
        code: '',
        firstName: '',
        lastName: '',
        email: '',
        phoneNumber: '',
        account: defaultAccount(),
        location: defaultLocation(),
    }
}
