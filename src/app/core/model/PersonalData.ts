import { Account } from './Account';
import { Location } from './Location';

export interface PersonalData {
    code: string;
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: string;
    account: Account;
    location: Location;
}
