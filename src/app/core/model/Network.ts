export interface Network {
    name: string;
    code: string;
}
