import { Network } from '../Network';
import { AbstractMapper } from './AbstractMapper';
import { Injectable } from '@angular/core';

@Injectable()
export class NetworkMapper extends AbstractMapper<Network> {

    values(network: Network): string[] {
        return [network.code, network.name];
    }

    labels(): string[] {
        return ['labels.network.code', 'labels.network.name'];
    }
}
