import { AbstractMapper } from './AbstractMapper';
import { Announcement } from '../Announcement';
import { Injectable } from '@angular/core';

@Injectable()
export class AnnouncementMapper extends AbstractMapper<Announcement> {

    values(announcement: Announcement): string[] {
        return [announcement.code, announcement.title, announcement.description,
            this.makeNameCodeNetworkRecord(announcement.fromCryptocurrency?.name, announcement.fromCryptocurrency?.network?.code),
            this.makeString(announcement.fromAmount),
            this.makeNameCodeNetworkRecord(announcement.toCryptocurrency?.name, announcement.toCryptocurrency?.network?.code),
            this.makeString(announcement.toAmount), this.generateRate(announcement.fromAmount, announcement.toAmount, announcement.fromCryptocurrency?.decimals)];
    }

    labels(): string[] {
        return ['labels.announcement.code', 'labels.announcement.title', 'labels.announcement.description',
            'labels.announcement.fromCryptocurrency', 'labels.announcement.fromAmount', 'labels.announcement.toCryptocurrency',
            'labels.announcement.toAmount', 'labels.announcement.rate'];
    }

    makeNameCodeNetworkRecord(name: string | undefined, code: string | undefined): string {
        if (name && code) {
            return name + '/' + code;
        }

        return '';
    }

    generateRate(from: number, to: number, decimals: number): string {
        return (to / from).toFixed(decimals ? decimals : 2) + '';
    }
}
