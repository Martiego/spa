import { AbstractMapper } from './AbstractMapper';
import { Cryptocurrency } from '../Cryptocurrency';
import { Injectable } from '@angular/core';

@Injectable()
export class CryptocurrencyMapper extends AbstractMapper<Cryptocurrency> {

    values(cryptocurrency: Cryptocurrency): string[] {
        return [cryptocurrency.name, cryptocurrency.code, this.ifNotNull(cryptocurrency.contract), this.makeString(cryptocurrency.decimals)];
    }

    labels(): string[] {
        return ['labels.cryptocurrency.name', 'labels.cryptocurrency.code', 'labels.cryptocurrency.contract',
            'labels.cryptocurrency.decimals'];
    }
}
