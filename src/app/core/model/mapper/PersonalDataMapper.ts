import { AbstractMapper } from './AbstractMapper';
import { PersonalData } from '../PersonalData';
import { Injectable } from '@angular/core';

@Injectable()
export class PersonalDataMapper extends AbstractMapper<PersonalData> {

    values(personalData: PersonalData): string[] {
        return [personalData.firstName, personalData.lastName, personalData.phoneNumber, personalData.email];
    }

    labels(): string[] {
        return ['labels.personalData.firstName', 'labels.personalData.lastName', 'labels.personalData.phoneNumber',
            'labels.personalData.email'];
    }
}
