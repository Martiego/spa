export abstract class AbstractMapper<D> {

    values(model: D): string[] {
        return [];
    }

    labels(): string [] {
        return [];
    }

    protected makeString(value: number): string {
        return value + '';
    }

    protected ifNotNull(value: any): string {
        return value ? value as string : '';
    }
}
