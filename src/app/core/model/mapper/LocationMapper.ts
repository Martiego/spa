import { Location } from '../Location';
import { AbstractMapper } from './AbstractMapper';
import { Injectable } from '@angular/core';

@Injectable()
export class LocationMapper extends AbstractMapper<Location> {

    values(location: Location): string[] {
        return [location.code, location.name];
    }

    labels(): string[] {
        return ['labels.location.code', 'labels.location.name'];
    }
}
