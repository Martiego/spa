import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { CookieService } from 'ngx-cookie-service';
import jwtDecode from 'jwt-decode';
import { Router } from '@angular/router';
import { PersonalData } from '../model/PersonalData';
import { Account } from '../model/Account';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    timeout = 5000;

    constructor(private http: HttpClient,
                private cookieService: CookieService,
                private router: Router) {
    }

    logIn(username: string, password: string): void {
        const body = {
            username: username,
            password: password
        };

        this.http.post(environment.url + 'auth', body, {observe: 'body', responseType: 'text'})
            .subscribe(jwt => {
                this.setInformationAboutUser(jwt);
                this.router.navigateByUrl('main');
            });
    }

    registerClient(personalData: PersonalData): void {
        this.http.post(environment.url + 'register/client', personalData)
            .subscribe(_ => this.router.navigateByUrl('main'));
    }

    registerAdmin(personalData: PersonalData): void {
        this.http.post(environment.url + 'register/admin', personalData)
            .subscribe(_ => this.router.navigateByUrl('main'));
    }

    resetPassword(username: string): void {
        const body = {
            username: username
        };

        this.http.post(environment.url + 'reset', body)
            .subscribe(_ => this.router.navigateByUrl(''));
    }

    activateAccount(oneTimeCode: string): void {
        this.http.get(environment.url + 'activate/' + oneTimeCode)
            .subscribe(_ => setTimeout(() => this.router.navigateByUrl(''), this.timeout));
    }

    activateAccountByAdmin(username: string): void {
        this.http.put(environment.url + 'activate/' + username, {})
            .subscribe(_ => this.router.navigateByUrl(''));
    }

    deactivateAccount(username: string): Observable<any> {
        return this.http.delete(environment.url + 'deactivate/' + username);
    }

    setNewPassword(account: Account, oneTimeCode: string): void {
        this.http.post(environment.url + 'password/' + oneTimeCode, account)
            .subscribe(_ => this.router.navigateByUrl(''));
    }

    logOut(): void {
        this.clearCookies();

        this.router.navigateByUrl('login');
    }

    setInformationAboutUser(jwt: string): void {
        const tokenInfo: any = jwtDecode(jwt);

        this.clearCookies();

        this.cookieService.set('jwt', jwt);
        this.cookieService.set('role', tokenInfo?.role);
        this.cookieService.set('sub', tokenInfo?.sub);
    }

    getJwt(): string {
        const jwt = this.cookieService.get('jwt');

        if (!this.isJwtValid(jwt)) {
            this.clearCookies();
            return "";
        }

        return jwt;
    }

    getUsername(): string {
        return this.cookieService.get('sub');
    }

    isLoggedIn(): boolean {
        const jwt = this.cookieService.get('jwt');
        return this.isJwtValid(jwt);
    }

    isClient(): boolean {
        return this.cookieService.get('role') === 'ROLE_CLIENT';
    }

    isAdmin(): boolean {
        return this.cookieService.get('role') === 'ROLE_ADMIN';
    }

    isClientOrAdmin(): boolean {
        return this.isClient() || this.isAdmin();
    }

    isJwtValid(jwt: string) {
        if (!jwt) {
            return false;
        }

        const tokenInfo: any = jwtDecode(jwt);
        return tokenInfo?.exp > new Date().getTime() / 1000;
    }

    clearCookies(): void {
        this.cookieService.set('jwt', '');
        this.cookieService.set('role', '');
        this.cookieService.set('sub', '');
    }
}
