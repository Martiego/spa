import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cryptocurrency } from '../model/Cryptocurrency';
import { AbstractService } from './abstract.service';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
    providedIn: 'root'
})
export class CryptocurrencyService extends AbstractService<Cryptocurrency> {

    constructor(http: HttpClient,
                cookieService: CookieService) {
        super(http, cookieService,'cryptocurrency', 'cryptocurrencies');
    }
}
