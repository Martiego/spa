import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Network } from '../model/Network';
import { AbstractService } from './abstract.service';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
    providedIn: 'root'
})
export class NetworkService extends AbstractService<Network> {

    constructor(http: HttpClient,
                cookieService: CookieService) {
        super(http, cookieService, 'network', 'networks');
    }
}
