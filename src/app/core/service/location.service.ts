import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Location } from '../model/Location';
import { AbstractService } from './abstract.service';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
    providedIn: 'root'
})
export class LocationService extends AbstractService<Location> {

    constructor(http: HttpClient,
                cookieService: CookieService) {
        super(http, cookieService, 'location', 'locations');
    }
}
