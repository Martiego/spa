import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Announcement } from '../model/Announcement';
import { AbstractService } from './abstract.service';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
    providedIn: 'root'
})
export class AnnouncementService extends AbstractService<Announcement> {

    constructor(http: HttpClient,
                cookieService: CookieService) {
        super(http, cookieService, 'announcement', 'announcements');
    }
}
