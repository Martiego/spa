import { AbstractService } from './abstract.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { PersonalData } from '../model/PersonalData';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
    providedIn: 'root'
})
export class PersonalDataService extends AbstractService<PersonalData> {

    constructor(http: HttpClient,
                cookieService: CookieService,
                private authService: AuthService) {
        super(http, cookieService, 'personalData', 'personalData');
    }

    delete(code: string): Observable<PersonalData> {
        return this.authService.deactivateAccount(code);
    }
}
