import { Observable } from 'rxjs';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { CookieService } from 'ngx-cookie-service';

export abstract class AbstractService<D> {

    protected constructor(protected http: HttpClient,
                          private cookieService: CookieService,
                          private readonly singular: string,
                          private readonly plural: string) {
        this.singular = singular;
        this.plural = plural;
    }

    getAll(): Observable<D[]> {
        return this.http.get<D[]>(environment.url + this.plural);
    }

    get(code: string): Observable<HttpResponse<D>> {
        return this.http.get<D>(environment.url + this.singular + '/' + code, {
            observe: 'response'
        });
    }

    delete(code: string): Observable<D> {
        return this.http.delete<D>(environment.url + this.singular + '/' + code);
    }

    add(entity: D): Observable<HttpResponse<D>> {
        return this.http.post<D>(environment.url + this.singular, entity,{
            observe: 'response'
        });
    }

    edit(entity: D): Observable<any> {
        return this.http.put(environment.url + this.singular, entity, {
            headers: { 'If-Match': this.cookieService.get('eTag').replace('\"', '') }
        });
    }
}
