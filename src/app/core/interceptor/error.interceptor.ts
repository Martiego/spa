import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ErrorModalComponent } from '../../utils/error-modal/error-modal.component';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

    constructor(private modalService: NgbModal) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler)
        : Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            catchError(error => {
                const modalRef = this.modalService.open(ErrorModalComponent);
                modalRef.componentInstance.message = error.error;

                return throwError(error.error);
            })
        );
    }

}
