import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
    selector: 'form-errors',
    templateUrl: './error.component.html',
    styleUrls: ['./error.component.less']
})
export class ErrorComponent implements OnInit {

    @Input()
    control: AbstractControl | undefined;

    constructor() {
    }

    ngOnInit(): void {
    }

}
