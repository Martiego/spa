import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-details',
    templateUrl: './details.component.html',
    styleUrls: ['./details.component.less']
})
export class DetailsComponent implements OnInit {

    @Input()
    labels: string[] = [];

    @Input()
    values: string[] = [];

    constructor() {
    }

    ngOnInit(): void {
    }

}
