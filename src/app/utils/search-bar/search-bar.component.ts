import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-search-bar',
    templateUrl: './search-bar.component.html',
    styleUrls: ['./search-bar.component.less']
})
export class SearchBarComponent implements OnInit {

    model: string = '';

    @Input()
    values: any[] = [];

    @Input()
    by: string = '';

    @Output()
    valuesEvent: EventEmitter<any> = new EventEmitter<any>();

    search(): void {
        this.valuesEvent.emit(this.filterValues());
    }

    constructor() {
    }

    ngOnInit(): void {
    }

    filterValues(): any[] {
        return this.values.filter(val => (val[this.by] as string).toLocaleLowerCase().includes(this.model.toLowerCase()));
    }

}
