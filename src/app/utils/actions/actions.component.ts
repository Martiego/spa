import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-actions',
    templateUrl: './actions.component.html',
    styleUrls: ['./actions.component.less']
})
export class ActionsComponent implements OnInit {

    @Input()
    add: boolean = false;

    @Input()
    edit: boolean = false;

    @Input()
    delete: boolean = false;

    @Input()
    deactivate: boolean | undefined = false;

    @Input()
    activate: boolean = false;

    @Output()
    deleteEvent: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    activateEvent: EventEmitter<any> = new EventEmitter<any>();

    constructor() {
    }

    ngOnInit(): void {
    }

    deleteObject(): void {
        this.deleteEvent.emit();
    }

    activateObject(): void {
        this.activateEvent.emit();
    }
}
