import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/service/auth.service';

@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.less']
})
export class NavigationComponent implements OnInit {

    constructor(public authService: AuthService) {
    }

    ngOnInit(): void {
    }

    logOut(): void {
        this.authService.logOut();
    }

}
