import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-login-navigation',
    templateUrl: './login-navigation.component.html',
    styleUrls: ['./login-navigation.component.less']
})
export class LoginNavigationComponent implements OnInit {

    @Input()
    reset: boolean = false;

    @Input()
    register: boolean = false;

    @Input()
    announcement: boolean = false;

    @Input()
    logIn: boolean = false;

    constructor() {
    }

    ngOnInit(): void {
    }

}
