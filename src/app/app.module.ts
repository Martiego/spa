import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './authorization/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { CryptocurrenciesResolver } from './core/resolver/cryptocurrencies/cryptocurrencies.resolver';
import { AnnouncementsResolver } from './core/resolver/announcements/announcements.resolver';
import { NetworksResolver } from './core/resolver/networks/networks.resolver';
import { AuthGuard } from './core/guard/auth.guard';
import { LocationsResolver } from './core/resolver/locations/locations.resolver';
import { LocationsTableComponent } from './main-components/locations/locations-table/locations-table.component';
import { LocationComponent } from './main-components/locations/location/location.component';
import { LocationResolver } from './core/resolver/locations/location.resolver';
import { NetworksTableComponent } from './main-components/networks/networks-table/networks-table.component';
import { NetworkComponent } from './main-components/networks/network/network.component';
import { NetworkResolver } from './core/resolver/networks/network.resolver';
import { CryptocurrenciesTableComponent } from './main-components/cryptocurrencies/cryptocurrencies-table/cryptocurrencies-table.component';
import { CryptocurrencyComponent } from './main-components/cryptocurrencies/cryptocurrency/cryptocurrency.component';
import { CryptocurrencyResolver } from './core/resolver/cryptocurrencies/cryptocurrency.resolver';
import { MainPageComponent } from './main-page/main-page.component';
import { AnnouncementsTableComponent } from './main-components/announcements/announcements-table/announcements-table.component';
import { AnnouncementComponent } from './main-components/announcements/announcement/announcement.component';
import { AnnouncementResolver } from './core/resolver/announcements/announcement.resolver';
import { AddLocationComponent } from './main-components/locations/add-location/add-location.component';
import { AddNetworkComponent } from './main-components/networks/add-network/add-network.component';
import { AddCryptocurrencyComponent } from './main-components/cryptocurrencies/add-cryptocurrency/add-cryptocurrency.component';
import { AddAnnouncementComponent } from './main-components/announcements/add-announcement/add-announcement.component';
import { EditLocationComponent } from './main-components/locations/edit-location/edit-location.component';
import { EditNetworkComponent } from './main-components/networks/edit-network/edit-network.component';
import { ErrorComponent } from './utils/error/error.component';
import { EditCryptocurrencyComponent } from './main-components/cryptocurrencies/edit-cryptocurrency/edit-cryptocurrency.component';
import { ClientGuard } from './core/guard/client.guard';
import { AdminGuard } from './core/guard/admin.guard';
import { ResetPasswordComponent } from './authorization/reset-password/reset-password.component';
import { RegisterComponent } from './authorization/register/register.component';
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { TranslateLoader, TranslateModule, TranslateService } from "@ngx-translate/core";
import { AdminClientGuard } from './core/guard/admin-client.guard';
import { ActivateAccountComponent } from './authorization/active-account/activate-account.component';
import { SetPasswordComponent } from './authorization/set-password/set-password.component';
import { PersonalDataTableComponent } from './main-components/personal-data/personal-data-table/personal-data-table.component';
import { AllPersonalDataResolver } from './core/resolver/personal-data/all-personal-data.resolver';
import { AuthInterceptor } from './core/interceptor/auth.interceptor';
import { PersonalDataComponent } from './main-components/personal-data/personal-data/personal-data.component';
import { PersonalDataResolver } from './core/resolver/personal-data/personal-data.resolver';
import { EditPersonalDataComponent } from './main-components/personal-data/edit-personal-data/edit-personal-data.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavigationComponent } from './utils/navigation/navigation.component';
import { ConfirmComponent } from './utils/confirm/confirm.component';
import { DetailsComponent } from './utils/details/details.component';
import { AnnouncementMapper } from './core/model/mapper/AnnouncementMapper';
import { CryptocurrencyMapper } from './core/model/mapper/CryptocurrencyMapper';
import { LocationMapper } from './core/model/mapper/LocationMapper';
import { NetworkMapper } from './core/model/mapper/NetworkMapper';
import { PersonalDataMapper } from './core/model/mapper/PersonalDataMapper';
import { ActionsComponent } from './utils/actions/actions.component';
import { SearchBarComponent } from './utils/search-bar/search-bar.component';
import { ContactComponent } from './main-components/announcements/contact/contact.component';
import { ErrorInterceptor } from './core/interceptor/error.interceptor';
import { ErrorModalComponent } from './utils/error-modal/error-modal.component';
import { LoginNavigationComponent } from './utils/login-navigation/login-navigation.component';

export function rootLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http)
}

export const httpInterceptorProviders = [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
];

export const mappers = [
    AnnouncementMapper,
    CryptocurrencyMapper,
    LocationMapper,
    NetworkMapper,
    PersonalDataMapper
];

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        LocationsTableComponent,
        LocationComponent,
        AddLocationComponent,
        NetworksTableComponent,
        NetworkComponent,
        CryptocurrenciesTableComponent,
        CryptocurrencyComponent,
        MainPageComponent,
        AnnouncementsTableComponent,
        AnnouncementComponent,
        AddNetworkComponent,
        AddCryptocurrencyComponent,
        AddAnnouncementComponent,
        EditLocationComponent,
        EditNetworkComponent,
        ErrorComponent,
        EditCryptocurrencyComponent,
        ResetPasswordComponent,
        RegisterComponent,
        ActivateAccountComponent,
        SetPasswordComponent,
        PersonalDataTableComponent,
        PersonalDataComponent,
        EditPersonalDataComponent,
        NavigationComponent,
        ConfirmComponent,
        DetailsComponent,
        ActionsComponent,
        SearchBarComponent,
        ContactComponent,
        ErrorModalComponent,
        LoginNavigationComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        NgbModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: rootLoaderFactory,
                deps: [HttpClient]
            }
        })
    ],
    providers: [
        CryptocurrenciesResolver,
        CryptocurrencyResolver,
        AnnouncementsResolver,
        AnnouncementResolver,
        NetworksResolver,
        NetworkResolver,
        LocationsResolver,
        LocationResolver,
        AuthGuard,
        AdminGuard,
        ClientGuard,
        AdminClientGuard,
        TranslateService,
        AllPersonalDataResolver,
        PersonalDataResolver,
        httpInterceptorProviders,
        ...mappers
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
