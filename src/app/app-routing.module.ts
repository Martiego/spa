import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './authorization/login/login.component';
import { CryptocurrenciesResolver } from './core/resolver/cryptocurrencies/cryptocurrencies.resolver';
import { AnnouncementsResolver } from './core/resolver/announcements/announcements.resolver';
import { NetworksResolver } from './core/resolver/networks/networks.resolver';
import { AuthGuard } from './core/guard/auth.guard';
import { LocationsResolver } from './core/resolver/locations/locations.resolver';
import { LocationsTableComponent } from './main-components/locations/locations-table/locations-table.component';
import { LocationComponent } from './main-components/locations/location/location.component';
import { LocationResolver } from './core/resolver/locations/location.resolver';
import { NetworksTableComponent } from './main-components/networks/networks-table/networks-table.component';
import { NetworkComponent } from './main-components/networks/network/network.component';
import { NetworkResolver } from './core/resolver/networks/network.resolver';
import { CryptocurrenciesTableComponent } from './main-components/cryptocurrencies/cryptocurrencies-table/cryptocurrencies-table.component';
import { CryptocurrencyComponent } from './main-components/cryptocurrencies/cryptocurrency/cryptocurrency.component';
import { CryptocurrencyResolver } from './core/resolver/cryptocurrencies/cryptocurrency.resolver';
import { MainPageComponent } from './main-page/main-page.component';
import { AnnouncementsTableComponent } from './main-components/announcements/announcements-table/announcements-table.component';
import { AnnouncementComponent } from './main-components/announcements/announcement/announcement.component';
import { AnnouncementResolver } from './core/resolver/announcements/announcement.resolver';
import { AddLocationComponent } from './main-components/locations/add-location/add-location.component';
import { AddNetworkComponent } from './main-components/networks/add-network/add-network.component';
import { AddCryptocurrencyComponent } from './main-components/cryptocurrencies/add-cryptocurrency/add-cryptocurrency.component';
import { AddAnnouncementComponent } from './main-components/announcements/add-announcement/add-announcement.component';
import { EditLocationComponent } from './main-components/locations/edit-location/edit-location.component';
import { EditNetworkComponent } from './main-components/networks/edit-network/edit-network.component';
import { EditCryptocurrencyComponent } from './main-components/cryptocurrencies/edit-cryptocurrency/edit-cryptocurrency.component';
import { ClientGuard } from './core/guard/client.guard';
import { ResetPasswordComponent } from './authorization/reset-password/reset-password.component';
import { RegisterComponent } from './authorization/register/register.component';
import { AdminGuard } from './core/guard/admin.guard';
import { AdminClientGuard } from './core/guard/admin-client.guard';
import { ActivateAccountComponent } from './authorization/active-account/activate-account.component';
import { SetPasswordComponent } from './authorization/set-password/set-password.component';
import { PersonalDataTableComponent } from './main-components/personal-data/personal-data-table/personal-data-table.component';
import { AllPersonalDataResolver } from './core/resolver/personal-data/all-personal-data.resolver';
import { PersonalDataResolver } from './core/resolver/personal-data/personal-data.resolver';
import { PersonalDataComponent } from './main-components/personal-data/personal-data/personal-data.component';
import { EditPersonalDataComponent } from './main-components/personal-data/edit-personal-data/edit-personal-data.component';

const personalData: Routes = [
    {
        path: 'personaldata',
        component: PersonalDataTableComponent,
        canActivate: [AuthGuard, AdminGuard],
        resolve: {
            personalData: AllPersonalDataResolver
        }
    },
    {
        path: 'personaldata/add',
        component: RegisterComponent,
        canActivate: [AuthGuard, AdminGuard]
    },
    {
        path: 'personaldata/:code',
        component: PersonalDataComponent,
        canActivate: [AuthGuard, AdminClientGuard],
        resolve: {
            personalData: PersonalDataResolver
        }
    },
    {
        path: 'personaldata/:code/edit',
        component: EditPersonalDataComponent,
        canActivate: [AuthGuard, AdminClientGuard],
        resolve: {
            personalData: PersonalDataResolver
        }
    }
]

const locations: Routes = [
    {
        path: 'locations',
        component: LocationsTableComponent,
        canActivate: [AuthGuard, AdminClientGuard],
        resolve: {
            locations: LocationsResolver
        }
    },
    {
        path: 'locations/add',
        component: AddLocationComponent,
        canActivate: [AuthGuard, AdminGuard],
    },
    {
        path: 'locations/:code',
        component: LocationComponent,
        canActivate: [AuthGuard, AdminClientGuard],
        resolve: {
            location: LocationResolver
        }
    },
    {
        path: 'locations/:code/edit',
        component: EditLocationComponent,
        canActivate: [AuthGuard, AdminGuard],
        resolve: {
            location: LocationResolver
        }
    }
];

const networks: Routes = [
    {
        path: 'networks',
        component: NetworksTableComponent,
        canActivate: [AuthGuard, AdminClientGuard],
        resolve: {
            networks: NetworksResolver
        }
    },
    {
        path: 'networks/add',
        component: AddNetworkComponent,
        canActivate: [AuthGuard, AdminGuard],
    },
    {
        path: 'networks/:code',
        component: NetworkComponent,
        canActivate: [AuthGuard, AdminClientGuard],
        resolve: {
            network: NetworkResolver
        }
    },
    {
        path: 'networks/:code/edit',
        component: EditNetworkComponent,
        canActivate: [AuthGuard, AdminGuard],
        resolve: {
            network: NetworkResolver
        }
    }
];

const cryptocurrencies: Routes = [
    {
        path: 'cryptocurrencies',
        component: CryptocurrenciesTableComponent,
        canActivate: [AuthGuard, AdminClientGuard],
        resolve: {
            cryptocurrencies: CryptocurrenciesResolver
        }
    },
    {
        path: 'cryptocurrencies/add',
        component: AddCryptocurrencyComponent,
        canActivate: [AuthGuard, AdminGuard],
        resolve: {
            networks: NetworksResolver
        }
    },
    {
        path: 'cryptocurrencies/:code',
        component: CryptocurrencyComponent,
        canActivate: [AuthGuard, AdminClientGuard],
        resolve: {
            cryptocurrency: CryptocurrencyResolver
        }
    },
    {
        path: 'cryptocurrencies/:code/edit',
        component: EditCryptocurrencyComponent,
        canActivate: [AuthGuard, AdminGuard],
        resolve: {
            cryptocurrency: CryptocurrencyResolver
        }
    }
];

const announcements: Routes = [
    {
        path: 'announcements',
        component: AnnouncementsTableComponent,
        canActivate: [],
        resolve: {
            announcements: AnnouncementsResolver
        }
    },
    {
        path: 'announcements/add',
        component: AddAnnouncementComponent,
        canActivate: [AuthGuard, ClientGuard],
        resolve: {
            locations: LocationsResolver,
            cryptocurrencies: CryptocurrenciesResolver
        }
    },
    {
        path: 'announcements/:code',
        component: AnnouncementComponent,
        canActivate: [],
        resolve: {
            announcement: AnnouncementResolver
        }
    }
];

const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent,
        canActivate: [],
    },
    {
        path: 'reset',
        component: ResetPasswordComponent,
        canActivate: [],
    },
    {
        path: 'register',
        component: RegisterComponent,
        canActivate: [],
    },
    {
        path: 'activate/:oneTimeCode',
        component: ActivateAccountComponent,
        canActivate: [],
    },
    {
        path: 'password/:oneTimeCode',
        component: SetPasswordComponent,
        canActivate: [],
    },
    {
        path: 'main',
        component: MainPageComponent,
        canActivate: [],
        children: [
            ...personalData,
            ...locations,
            ...networks,
            ...cryptocurrencies,
            ...announcements
        ]
    },
    {
        path: '**',
        redirectTo: 'login'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
